# Copyright 2009 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require java [ virtual_jdk_and_jre_deps=true ]

SUMMARY="Analyze, create, and manipulate Java class files"
DESCRIPTION="
The Byte Code Engineering Library is intended to give users a convenient
possibility to analyze, create, and manipulate (binary) Java class files. Classes
are represented by objects which contain all the symbolic information of the given
class: methods, fields and byte code instructions, in particular. Such objects can
be read from an existing file, be transformed by a program (e.g. a class loader
at run-time) and dumped to a file again. An even more interesting application is
the creation of classes from scratch at run-time. BCEL contains a byte code
verifier named JustIce, which usually gives you much better information about
what's wrong with your code than the standard JVM message.
"
HOMEPAGE="http://commons.apache.org/proper/commons-${PN}"
DOWNLOADS="mirror://apache/commons/${PN}/source/${PNV}-src.tar.gz"

BUGS_TO="philantrop@exherbo.org"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/manual/manual.html [[ lang = en ]]"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES=""

WORK=${WORKBASE}/${PNV}-src

JAVA_SRC_DIR="src/main/java/org/apache/bcel"

src_compile() {
     local sources=sources.lst
     local classes=target/classes

     edo find ${JAVA_SRC_DIR:-*} -name \*.java > ${sources}
     edo mkdir -p ${classes}
     edo javac -verbose -d ${classes} -encoding UTF-8 @${sources}
     edo jar cfv ${PN}.jar -C ${classes} .
}

src_install() {
    dodir /usr/share/${PN}
    insinto /usr/share/${PN}
    newins ${PN}.jar ${PNV}.jar
}

